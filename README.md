<!-- PROJECT LOGO -->
<br />
<p align="center">
  <h3 align="center">Projector TIG</h3>

  <p align="center">
    Проект содержит docker-compose файл для разворачивания всего стека (TIG + golang app + node app + mongo + elastic).
    <br />
  </p>
</p>

<!-- Как запускать -->
### Запускаем все контейнеры
Команда поднимет docker-compose и зальёт в базы данные.
  ```sh
  make init
  ```

Приложения будут запущены на следующих портах
  ```makefile
APP_NODE_PORT = 8080
APP_GO_PORT   = 8081

GRAFANA_PORT  = 3000
  ```

Go апка ходит только в монгу, node апка ходит также в эластику.

```sh
ab -n 1000 -c 5 -t 60 -s 10 http://localhost:8080/fetch
ab -n 1000 -c 5 -t 60 -s 10 http://localhost:8081/fetch
```

Графики после нагрузки
![Alt text](./img.png "Optional Title")