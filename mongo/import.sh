#!/usr/bin/env bash

# Default parameters value
DOCKER=0
SMALL=0
URI=mongodb://localhost

usage() {
    echo "Usage: ./import.sh [-h|-help] [-d|--docker] [-s|--small] [-u URI|-u=URI|--uri URI|--uri=URI]"
    echo
    echo "OPTIONS:"
    echo -e "  -d, --docker\t starts a MongoDB docker container on port 27017 with --rm option."
    echo -e "  -u, --uri\t MongoDB Cluster URI. Can contain user:password if --auth is activated."
    echo -e "\t\t Compatible with \"mongodb://\" and \"mongodb://srv\" connection strings."
    echo -e "  -h, --help\t prints the help."
    echo
    echo "EXAMPLES:"
    echo "  ./import.sh"
    echo "  ./import.sh -h"
    echo "  ./import.sh --help"
    echo "  ./import.sh --docker"
    echo "  ./import.sh -d"
    echo "  ./import.sh -u=mongodb+srv://user:password@freecluster-abcde.mongodb.net"
    echo "  ./import.sh --docker --small --uri mongodb://127.0.0.1"
    echo "  ./import.sh -d -s --uri mongodb://127.0.0.1"
    echo "  ./import.sh -d -s --uri=mongodb://127.0.0.1"
    echo "  ./import.sh -ds --uri mongodb://127.0.0.1"
    echo "  ./import.sh -ds -u mongodb://127.0.0.1"
    echo "  ./import.sh -ds -u=mongodb://127.0.0.1"
    echo "  ./import.sh -dsu mongodb://127.0.0.1"
    echo "  ./import.sh -dsu=mongodb://127.0.0.1"
    echo "  ./import.sh -sd --uri mongodb://127.0.0.1"
    echo "  ./import.sh -sd -u mongodb://127.0.0.1"
    echo "  ./import.sh -sd -u=mongodb://127.0.0.1"
    echo "  ./import.sh -sdu mongodb://127.0.0.1"
    echo "  ./import.sh -sdu=mongodb://127.0.0.1"
    exit 1
}

echo_error() {
    echo -e "\033[0;31m$1\033[0m"
}

argument_parsing() {
    while [[ $# -gt 0 ]]; do
        case "$1" in
            -h | --help)
                usage
                exit 0
            ;;
            -d | --docker)
                DOCKER=1
            ;;
            -s | --small)
                SMALL=1
            ;;
            -ds | -sd)
                DOCKER=1
                SMALL=1
            ;;
            -u | --uri)
                shift
                URI="$1"
            ;;
            -dsu | -sdu)
                shift
                DOCKER=1
                SMALL=1
                URI="$1"
            ;;
            -u=* | --uri=*)
                URI="${1#*=}"
            ;;
            -dsu=* | -sdu=*)
                DOCKER=1
                SMALL=1
                URI="${1#*=}"
            ;;
            *)
                echo_error "Unknown option '$1'"
                usage
            ;;
        esac
        shift
    done
}

import_big_datasets() {
    if [[ ${SMALL} -eq 0 ]]; then
        cd datasets

        unzip people-bson.zip -d dump
        mongorestore --gzip --drop --uri ${URI}
        rm -rf dump

        cd ..
    fi
}

argument_parsing $@
import_big_datasets
