mongo_ip:
	$(eval MONGO_IP:=$(shell docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' leb-mongo))
	@echo $(MONGO_IP)

init: up prepare

.PHONY: up
up:
	docker-compose --profile=app up -d

.PHONY: prepare
prepare: mongo_ip
	sleep 5
	docker exec leb-mongo mongo --eval "rs.initiate();"
	docker start leb-mongo-2-elastic
	cd mongo && ./import.sh -u=mongodb://$(MONGO_IP)