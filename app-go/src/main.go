package main

import (
	"context"
	"encoding/json"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"
)

type handlers struct {
	mongoClient *mongo.Client
}

func (h *handlers) Hello(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "hello\n")
}

func (h *handlers) Fetch(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "hello\n")

	type person struct {
		ID     string `json:"id" bson:"_id"`
		Name   string `json:"name" bson:"name"`
		Points int    `json:"points" bson:"points"`
	}

	coll := h.mongoClient.Database("people-bson").Collection("people")

	docsNumber, err := coll.CountDocuments(req.Context(), bson.M{})

	limit := rand.Int63n(1000)
	offset := docsNumber - limit

	c, err := coll.Find(
		req.Context(),
		bson.M{},
		options.Find().SetSkip(offset).SetLimit(limit).SetSort(bson.M{"signup": 1}).SetAllowDiskUse(true),
	)
	if err != nil {
		data, _ := json.Marshal(err)
		w.Write(data)
		w.WriteHeader(500)
		return
	}

	var people []person

	if err := c.All(req.Context(), &people); err != nil {
		data, _ := json.Marshal(err)
		w.Write(data)
		w.WriteHeader(500)
		return
	}

	data, _ := json.Marshal(people)
	w.Write(data)
	w.WriteHeader(200)
}

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	mongoHost := os.Getenv("MONGO_HOST")
	if mongoHost == "" {
		mongoHost = "mongo"
	}

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:27017", mongoHost)))
	if err != nil {
		log.Fatalln(err)
	}

	h := &handlers{mongoClient: client}

	http.HandleFunc("/", h.Hello)
	http.HandleFunc("/fetch", h.Fetch)
	http.ListenAndServe(":80", nil)
}
