const express = require('express')
const {MongoClient} = require("mongodb");
const { Client  } = require('@elastic/elasticsearch')
const PromClient = require('prom-client');

const elasticClient = new Client({ node: `http://${process.env.ELASTIC_HOST || 'elastic'}:9200` });

const uri = `mongodb://${process.env.MONGO_HOST || 'mongo'}?retryWrites=true&writeConcern=majority`;
const mongoClient = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

const prefix = 'app_node_';
const collectDefaultMetrics = PromClient.collectDefaultMetrics;
const Registry = PromClient.Registry;
const register = new Registry();
collectDefaultMetrics({prefix, register});

const app = express()
const port = process.env.PORT || 80

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.get('/fetch', async (req, res) => {
    const database = mongoClient.db('people-bson');
    const people = database.collection('people');

    const limit = getRandomInt(1000);
    const offset = getRandomInt((await people.countDocuments()) - limit);

    try {
        const result = await elasticClient.search({
            index: 'people-bson',
            body: {
                from: getRandomInt(10000 - limit),
                size: limit,
                query: {
                    "match_all": {}
                }
            }
        });

        res.end(JSON.stringify(await people.find({}, {allowDiskUse: true}).sort({signup: 1}).skip(offset).limit(limit).toArray()));
    } catch (e) {
        res.end(JSON.stringify({message: e.message}));
    }
})

app.get('/metrics', async (req, res) => {
    try {
        res.set('Content-Type', register.contentType);
        res.end(await register.metrics());
    } catch (ex) {
        res.status(500).end(ex);
    }
});

app.listen(port, async () => {
    await mongoClient.connect();
    console.log(`Example app listening at http://localhost:${port}`)
})
